import com.example.appointments.AppointmentsApplication;
import com.example.appointments.model.Event;
import com.example.appointments.model.Participant;
import com.example.appointments.repository.ParticipantRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest(classes = AppointmentsApplication.class)
public class PersistenceTests {

    @Autowired
    ParticipantRepository participantRepository;

    @Test
    @Transactional
    void testParticipantStorage() {
        Participant participant = new Participant();
        participant.setName("name");
        participant.setSurname("surname");

        Event event = new Event();
        event.setName("test-event");
        event.setLocation("here");
        participant.addEvent(event);

        Participant storedEntity = participantRepository.save(participant);
        Assert.notNull(storedEntity.getId(), "id should be stored in db");
    }
}
