package com.example.appointments.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EventDto {
    private UUID uuid;
    private String name;
    private String location;
    private Integer maxParticipants;
    private Integer availableSlots;
    private LocalDateTime time;
    private Set<ParticipantDto> participants = new HashSet<>();
}
