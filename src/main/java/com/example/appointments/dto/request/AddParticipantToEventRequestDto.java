package com.example.appointments.dto.request;
import com.example.appointments.dto.ParticipantDto;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddParticipantToEventRequestDto {

    @NotNull(message = "Participant is required")
    private ParticipantDto participant;

    @NotNull(message = "Event ID is required")
    private Long eventId;
}
