package com.example.appointments.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateEventDto {
    private UUID uuid;
    private String name;
    private String location;
    private Integer maxParticipants;
    private LocalDateTime time;
    private Integer availableSlots;
}
