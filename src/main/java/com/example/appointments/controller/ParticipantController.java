package com.example.appointments.controller;

import com.example.appointments.AppUtils;
import com.example.appointments.dto.ParticipantDto;
import com.example.appointments.exception.ParticipantNotFoundException;
import com.example.appointments.mapper.ParticipantMapper;
import com.example.appointments.model.Participant;
import com.example.appointments.service.ParticipantService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/participants")
public class ParticipantController {

    private final ParticipantService participantService;
    private final AppUtils utils;


    @Autowired
    public ParticipantController(ParticipantService participantService, AppUtils utils) {
        this.participantService = participantService;
        this.utils = utils;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ParticipantDto> getParticipantById(@PathVariable Long id, HttpServletRequest request) {
        Participant participant = participantService.getParticipantById(id).orElseThrow(() -> new ParticipantNotFoundException("Could not find participant with ID: " + id));
        return new ResponseEntity<>(ParticipantMapper.INSTANCE.participantToParticipantDTO(participant), utils.generateRefreshToken(request), HttpStatus.CREATED);
    }
}
