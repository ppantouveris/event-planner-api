package com.example.appointments.controller;

import com.example.appointments.dto.request.SignInRequestDto;
import com.example.appointments.dto.request.SignUpRequestDto;
import com.example.appointments.dto.request.TokenValidRequestDto;
import com.example.appointments.dto.response.JwtAuthenticationResponseDto;
import com.example.appointments.dto.response.TokenValidityResponseDto;
import com.example.appointments.service.AuthenticationService;
import com.example.appointments.service.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;
    private final JwtService jwtService;

    @PostMapping("/signup")
    public ResponseEntity<JwtAuthenticationResponseDto> signup(@RequestBody SignUpRequestDto request) {
        return ResponseEntity.ok(authenticationService.signup(request));
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtAuthenticationResponseDto> signIn(@RequestBody SignInRequestDto request) {
        return ResponseEntity.ok(authenticationService.signIn(request));
    }

    @PostMapping("/token/validate")
    public ResponseEntity<TokenValidityResponseDto> isTokenExpired(@RequestBody TokenValidRequestDto tokenValidRequestDto) {
        return ResponseEntity.ok(new TokenValidityResponseDto(jwtService.isTokenExpired(tokenValidRequestDto.getToken())));
    }
}
