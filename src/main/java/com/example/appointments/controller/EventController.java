package com.example.appointments.controller;

import com.example.appointments.AppUtils;
import com.example.appointments.dto.EventDto;
import com.example.appointments.dto.request.AddParticipantToEventRequestDto;
import com.example.appointments.dto.request.UpdateEventDto;
import com.example.appointments.mapper.EventMapper;
import com.example.appointments.model.Event;
import com.example.appointments.service.EventService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/events")
public class EventController {

    private final EventService eventService;
    private final AppUtils utils;

    @Autowired
    public EventController(EventService eventService, AppUtils utils) {
        this.eventService = eventService;
        this.utils = utils;
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<EventDto> getEventByUuid(@PathVariable("uuid") UUID uuid, HttpServletRequest request) {
        Event event = eventService.getEventByUuid(uuid);
        return new ResponseEntity<>(EventMapper.INSTANCE.eventToEventDTO(event), utils.generateRefreshToken(request), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<EventDto>> getAllEvents(HttpServletRequest request) {
        List<Event> allEvents = eventService.getAllEvents();
        return new ResponseEntity<>(EventMapper.INSTANCE.eventListToEventDTOList(allEvents), utils.generateRefreshToken(request), HttpStatus.CREATED);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<EventDto> createEvent(@RequestBody EventDto eventDTO, HttpServletRequest request) {
        EventDto createdEvent = eventService.createEvent(eventDTO);
        return new ResponseEntity<>(createdEvent, utils.generateRefreshToken(request), HttpStatus.CREATED);
    }

    @PutMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<EventDto> updateEvent(@RequestBody UpdateEventDto eventDTO, HttpServletRequest request) {
        EventDto updatedEvent = eventService.updateEvent(eventDTO);
        return new ResponseEntity<>(updatedEvent, utils.generateRefreshToken(request), HttpStatus.CREATED);
    }

    @PostMapping("/add/participant")
    public ResponseEntity<EventDto> addParticipantToEvent(@Valid @RequestBody AddParticipantToEventRequestDto participantToEventRequest, HttpServletRequest request) {
        return new ResponseEntity<>(eventService.addParticipantToEvent(participantToEventRequest), utils.generateRefreshToken(request), HttpStatus.CREATED);
    }

    @DeleteMapping("/{uuid}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<String> deleteEventByUuid(@PathVariable("uuid") UUID uuid, HttpServletRequest request) {
        eventService.deleteEvent(uuid);
        return new ResponseEntity<>(null, utils.generateRefreshToken(request), HttpStatus.OK);
    }
}
