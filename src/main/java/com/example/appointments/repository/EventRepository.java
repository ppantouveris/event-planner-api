package com.example.appointments.repository;

import com.example.appointments.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.EntityGraph;


public interface EventRepository extends JpaRepository<Event, Long> {

    @EntityGraph(attributePaths = {"participants"})
    Optional<Event> findByUuid(UUID uuid);
}
