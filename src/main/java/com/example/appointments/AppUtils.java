package com.example.appointments;

import com.example.appointments.service.JwtService;
import com.example.appointments.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class AppUtils {

    private final JwtService jwtService;
    private final UserService userService;

    @Autowired
    public AppUtils(JwtService jwtService, UserService userService) {
        this.jwtService = jwtService;
        this.userService = userService;
    }

    public HttpHeaders generateRefreshToken(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        String jwt = authHeader.substring(7);
        String userEmail = jwtService.extractUserName(jwt);
        UserDetails userDetails = userService.userDetailsService().loadUserByUsername(userEmail);
        String refreshToken = jwtService.generateToken(userDetails);

        // Create headers and set the refreshed JWT
        HttpHeaders headers = new HttpHeaders();
        headers.set("Refresh-Token", refreshToken);

        return headers;
    }
}
