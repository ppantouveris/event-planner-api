package com.example.appointments.model;

public enum Role {
    USER,
    ADMIN
}
