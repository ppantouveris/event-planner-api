package com.example.appointments.model;

import com.example.appointments.exception.EventIsFullException;
import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID")
    @Column(updatable = false, nullable = false, unique = true)
    private UUID uuid = UUID.randomUUID();

    @NotNull
    private String name;

    @NotNull
    private String location;

    @Min(value = 0)
    private Integer maxParticipants;

    @Min(value = 0)
    private Integer availableSlots;

    @ManyToMany(mappedBy = "events", fetch = FetchType.LAZY)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Participant> participants = new HashSet<>();

    @NotNull
    @Column(name = "time")
    private LocalDateTime time;

    public void decreaseAvailableSlots() {
        if(this.getAvailableSlots() == 0) throw new EventIsFullException("There are no more available slots for event with ID: " + this.getId());
        this.setAvailableSlots(availableSlots - 1);
    }
}
