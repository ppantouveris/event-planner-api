package com.example.appointments.service.impl;

import com.example.appointments.dto.EventDto;
import com.example.appointments.dto.request.AddParticipantToEventRequestDto;
import com.example.appointments.dto.request.UpdateEventDto;
import com.example.appointments.exception.EventNotFoundException;
import com.example.appointments.mapper.EventMapper;
import com.example.appointments.mapper.ParticipantMapper;
import com.example.appointments.model.Event;
import com.example.appointments.model.Participant;
import com.example.appointments.repository.EventRepository;
import com.example.appointments.repository.ParticipantRepository;
import com.example.appointments.service.EventService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;
    private final ParticipantRepository participantRepository;

    @Autowired
    public EventServiceImpl(EventRepository eventRepository, ParticipantRepository participantRepository) {
        this.eventRepository = eventRepository;
        this.participantRepository = participantRepository;
    }

    @Override
    public Optional<Event> getEventById(Long id) {
        return eventRepository.findById(id);
    }

    @Override
    public Event getEventByUuid(UUID uuid) {
        return eventRepository.findByUuid(uuid).orElseThrow(() -> new EventNotFoundException("Event not found with UUID: " + uuid));
    }

    @Override
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @Override
    @Transactional
    public EventDto createEvent(EventDto eventDTO) {
        Event event = EventMapper.INSTANCE.eventDTOToEvent(eventDTO);
        Event createdEvent = eventRepository.save(event);
        return EventMapper.INSTANCE.eventToEventDTO(createdEvent);
    }

    @Override
    @Transactional
    public EventDto addParticipantToEvent(AddParticipantToEventRequestDto participantToEventRequest) {
        Event event = getEventById(participantToEventRequest.getEventId())
                .orElseThrow(() -> new EventNotFoundException("Event not found with ID: " + participantToEventRequest.getEventId()));

        Participant participant = ParticipantMapper.INSTANCE.participantDTOToParticipant(participantToEventRequest.getParticipant());
        event.decreaseAvailableSlots();
        participant.addEvent(event);
        participantRepository.save(participant);

        return EventMapper.INSTANCE.eventToEventDTO(event);
    }

    @Override
    @Transactional
    public EventDto updateEvent(UpdateEventDto eventDTO) {
        Event event = getEventByUuid(eventDTO.getUuid());
        EventMapper.INSTANCE.updateEventFromUpdateEventDto(eventDTO, event);
        Event updatedEvent = eventRepository.save(event);

        return EventMapper.INSTANCE.eventToEventDTO(updatedEvent);
    }

    @Override
    @Transactional
    public void deleteEvent(UUID uuid) {
        Event event = getEventByUuid(uuid);
        Set<Participant> participants = event.getParticipants();

        // Detach the event from its participants before deletion
        participants.forEach(participant -> participant.getEvents().remove(event));
        eventRepository.delete(event);

        // Now check each participant to see if they are part of any other event
        participants.forEach(participant -> {
            if (participant.getEvents().isEmpty()) {
                participantRepository.delete(participant);
            }
        });
    }
}
