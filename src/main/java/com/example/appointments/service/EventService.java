package com.example.appointments.service;

import com.example.appointments.dto.EventDto;
import com.example.appointments.dto.request.AddParticipantToEventRequestDto;
import com.example.appointments.dto.request.UpdateEventDto;
import com.example.appointments.model.Event;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EventService {
    Optional<Event> getEventById(Long id);

    Event getEventByUuid(UUID uuid);

    List<Event> getAllEvents();

    EventDto createEvent(EventDto eventDTO);

    EventDto addParticipantToEvent(AddParticipantToEventRequestDto participantToEventRequest);

    EventDto updateEvent(UpdateEventDto eventDTO);

    void deleteEvent(UUID uuid);
}
