package com.example.appointments.service;

import com.example.appointments.model.Participant;

import java.util.Optional;

public interface ParticipantService {
    Optional<Participant> getParticipantById(Long id);

}
