package com.example.appointments.service;

import com.example.appointments.dto.request.SignInRequestDto;
import com.example.appointments.dto.request.SignUpRequestDto;
import com.example.appointments.dto.response.JwtAuthenticationResponseDto;

public interface AuthenticationService {
    JwtAuthenticationResponseDto signup(SignUpRequestDto request);
    JwtAuthenticationResponseDto signIn(SignInRequestDto request);
}
