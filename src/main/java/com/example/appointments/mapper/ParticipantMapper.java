package com.example.appointments.mapper;

import com.example.appointments.dto.ParticipantDto;
import com.example.appointments.model.Participant;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ParticipantMapper {
    ParticipantMapper INSTANCE = Mappers.getMapper(ParticipantMapper.class);
    ParticipantDto participantToParticipantDTO(Participant participant);
    Participant participantDTOToParticipant(ParticipantDto dto);
}
