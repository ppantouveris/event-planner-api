package com.example.appointments.mapper;

import com.example.appointments.dto.EventDto;
import com.example.appointments.dto.request.UpdateEventDto;
import com.example.appointments.model.Event;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Mapper
public interface EventMapper {

    EventMapper INSTANCE = Mappers.getMapper(EventMapper.class);

    Event eventDTOToEvent(EventDto eventDTO);

    EventDto eventToEventDTO(Event event);

    void updateEventFromUpdateEventDto(UpdateEventDto dto, @MappingTarget Event event);

    default List<Event> eventDTOListToEventList(List<EventDto> eventDTOList) {
        return eventDTOList == null ? null :
                eventDTOList.stream()
                        .map(this::eventDTOToEvent)
                        .collect(Collectors.toList());
    }

    default List<EventDto> eventListToEventDTOList(List<Event> eventList) {
        return eventList == null ? null :
                eventList.stream()
                        .map(this::eventToEventDTO)
                        .collect(Collectors.toList());
    }

    @AfterMapping
    default void ensureUuidIsGenerated(EventDto eventDTO, @MappingTarget Event event) {
        if (event.getUuid() == null) {
            event.setUuid(UUID.randomUUID());
        }
    }
}
